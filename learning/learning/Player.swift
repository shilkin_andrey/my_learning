//
//  File.swift
//  learning
//
//  Created by Admin on 19.01.17.
//  Copyright © 2017 test_org. All rights reserved.
//

import Foundation

struct Player {
    var name: String?
    var game: String?
    var rating: Int
    
    init(name: String?, game: String?, rating: Int) {
        self.game = game
        self.name = name
        self.rating = rating
    }
}
