//
//  ViewController.swift
//  learning
//
//  Created by Admin on 18.01.17.
//  Copyright © 2017 test_org. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {

    var value: Int = 0
    
    @IBOutlet weak var slider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        value = lroundf(slider.value)
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnAction() {
    
        let message = "new value \(value)"
        
        let alert = UIAlertController(title: "Hello, World",
                                      message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func sliderMoved() {
        print("new value \(slider.value)")
        value = lroundf(slider.value)
    }

}

