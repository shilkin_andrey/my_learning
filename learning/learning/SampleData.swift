//
//  SampleData.swift
//  learning
//
//  Created by Admin on 19.01.17.
//  Copyright © 2017 test_org. All rights reserved.
//

import Foundation

let playersData = [ Player(name: "Bill", game: "Game 1", rating: 3),
Player(name: "Kame", game: "Game 2", rating: 1),
Player(name: "Nufla", game: "Game 3", rating: 5),]
